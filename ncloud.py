import psycopg2

def get_table(table, cols = '*', condition = None):
	'''
	Acquires the table from Nagro's cloud.
	INPUT:
		-table: table's name in the database. Could be  a string object or list, tuple or set of strings. If table is "all"
        -condition: filter to select on table. Should be a string object.
		-cols: The columns that will be requireds from the table. Needs to be a string, in MySQL format just how are write after the SELECT command. Default: 'geom'.
	OUTPUT:
		-result: the table acquired from the cloud as a list of tuples.
	'''
	if type(table) == str:
		print('Iniciando conexão com o banco de dados.')
		conn = psycopg2.connect(database='NagroGIS', host = '35.225.1.170', password = 'A6uY+7{G!Q', user = 'ndb')
		cursor = conn.cursor()
		cmd = 'SELECT ' + cols + ' FROM ' + table
		if condition != None:
			if type(condition) == str:
				cmd += ' WHERE ' + condition
			else:
				print('A condição inserida é inválida. (get_table)')
		cursor.execute(cmd)
		print('Selecionando elementos.')
		result = cursor.fetchall()
		return result
	else:
		return print('A tabela apresentada é inválida. (get_table)')