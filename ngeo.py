#Importando bibliotecas

import binascii #Convert ascii to binary
import os #Operational System lib
import rasterio #Raster reader
import numpy as np #Math lib
from geomet import wkb #Convert binary cto geo coordinates
from nagro import ncloud, nimg #Nagro libs
from sentinelsat.sentinel import SentinelAPI #Sat API
from pyunpack import Archive #Extract files
from tqdm import tqdm #Process stats bar
import robobrowser #Scrapper
import json #Its obvius
from shapely.geometry import box #Create a box to be clipped
import geopandas as gpd #Geodataframe
from fiona.crs import from_epsg #Adquire crs from epsg
import pycrs #Work with crs
from rasterio import mask #Mask to be clipped
from google.cloud import bigquery
from google.oauth2 import service_account
import requests


def hex2coord(hexa):
    '''
    Convert hexadecimals coordinates to geo coordinates (latitude & longitude).
    INPUT:
        -hexa: hexadecimal coordinates as a string.
    OUTPUT:
        -wkb_coords: geo coordinates as a dict with the keys ['type', 'coordinates', 'meta', 'crs'], where:
            -type: the type of the shape described by the coordinates as a string.
            -coordinates: the coordinates as a list of lists of lists of a list of coordinates pair as int.
            -meta: the metadata of the coordinates.
            -crs: a dict with the keys:
                -properties: a dict that has the key 'name' whose the value is the EPSG of the coordinates.
                -type: the type of the data included in the crs
    '''
    print('Convertendo coordenadas.')

    #Convert the hexadecimals coordinates to binary
    geocoords = binascii.unhexlify(hexa)

    #Convert binary to geo coordinates
    wkb_coords = wkb.loads(geocoords)

    return wkb_coords

def get_coordinates(table, condition = None, N = 0, ):
    '''
    Take the coordinates of a perimeter of a farm or a list of farms.
    INPUT:
        -table: table's name in the database. Could be  a string object or list, tuple or set of strings. If table is "all"
        -condition: filter to select on table. Should be a string object.
        -N = index of the selected raws. Should be a int object.
    OUTPUT:
        -coords: the geo coordinates of the perimeter of the farm as a string.
    '''

    #Download table(s) from the Nagro's cloud if table are a string, list, set or tuple
    #'geom' is the column of the table that has the coordinates of the farm
    if type(table) == str:
        table = ncloud.get_table(table, 'geom', condition)
    print('Baixando coordenadas.')
    if N == 'all':
        coords = list()
        
        for i in range(len(table)):
            coord = hex2coord(table[i][0])
            coords.append(coord)
        return coords
    
    elif type(N) == list or type(N) == set or type(N) == tuple:
        coords = list()
        for i in N:
            if type(i) == int:
                coord = hex2coord(table[i][0])
                coords.append(coord)
            else:
                return print('Valor de item inválido. (get_coordinates)')
        return coords

    elif type(N) == int:
        coord = hex2coord(table[N][0])
        return coord

    else:
        return print('Valor de item inválido. (get_coordinates)')

def coord2wkt(coord, form = 'POLYGON', p = 100):
    '''
    Write the geo coordinate as a wkt shape string.
    INPUT:
        -coord: the geo coordinate as wkt dict.
        -form: the type of the shape as a string.
        -p: the inverse percentage of the points that will be selected and returned. Default = 100, so the number of the
    points that will be returned is the total number of points devided by p. In this way, there's p poins between each
    selected points.
    OUTPUT:
        -coords_str: the string of the coordinates in wkt format.
    '''
    if type(coord) == dict:
        coords_list = coord['coordinates'][0][0] #Select the list of coordinates

    elif type(coord) == list:
        coords_list = coord

    coords_effective = list()
    for i in range(0, len(coords_list), p):
        temp = [round(coords_list[i][0],4), round(coords_list[i][1], 4)]
        coords_effective.append(temp)
    
    coords_str = 'POLYGON(('

    for point in coords_effective:
        coords_str += str(point[0]) + ' '
        coords_str += str(point[1]) + ','

    coords_str += '{} {}))'.format(str(coords_effective[0][0]), str(coords_effective[0][1]))

    return coords_str
def get_sent2_products(
    coords_str,
    user = 'thalesfreitaz',
    passw = 'sisustenido',
    link = 'https://scihub.copernicus.eu/dhus',
    begin = 'NOW-15DAYS',
    end = 'NOW',
    limit = 1,
    min_ = 1,
    percentcloud = (0, 3)
    ):
    '''
    Download pack with the 12 bands rasters by the sentinel 2 api.
    INPUT:
        -coords_str: the string of the coordinates in wkt format.
        -user: 
    '''

    try:
        api = SentinelAPI(user, passw, link)
    except:
        return print('Houve um erro no login. (download_sent2)')
    try:
        count = api.count(
            coords_str,
            date = (begin, end),
            platformname='Sentinel-2',
            limit = limit,
            cloudcoverpercentage = percentcloud
        )
        print('Número de produtos identificados:', count)
        #print(min_)
        if count < min_:
            return print('Número mínimo de produtos não atingido.')
    except:
        return print('Houve um erro na busca pelo produto. (download_sent2)')
    products = api.query(coords_str,
                        date = (begin, end),
                        platformname='Sentinel-2',
                        limit = limit,
                        cloudcoverpercentage = percentcloud
                        )
    return products

def download_sent2(
    coords_str,
    user = 'thalesfreitaz',
    passw = 'sisustenido',
    link = 'https://scihub.copernicus.eu/dhus',
    begin = 'NOW-15DAYS',
    end = 'NOW',
    limit = 1,
    min_ = 1,
    percentcloud = (0, 3)
    ):
    '''
    Download pack with the 12 bands rasters by the sentinel 2 api.
    INPUT:
        -coords_str: the string of the coordinates in wkt format.
        -user: 
    '''

    try:
        api = SentinelAPI(user, passw, link)
    except:
        return print('Houve um erro no login. (download_sent2)')
    try:
        count = api.count(
            coords_str,
            date = (begin, end),
            platformname='Sentinel-2',
            limit = limit,
            cloudcoverpercentage = percentcloud,
        )
        print('Número de produtos identificados:', count)
        #print(min_)
        if count < min_:
            return print('Número mínimo de produtos não atingido.')
    except:
        return print('Houve um erro na busca pelo produto. (download_sent2)')
    products = api.query(coords_str,
                        date = (begin, end),
                        platformname='Sentinel-2',
                        limit = limit,
                        cloudcoverpercentage = percentcloud
                        )
    #print(products)
    api.download_all(products)
    return True

def cloud2sat(
    table,
    N = 0,
    condition = None,
    form = 'POLYGON',
    p = 100,
    user = 'thalesfreitaz',
    passw = 'sisustenido',
    link = 'https://scihub.copernicus.eu/dhus',
    begin = 'NOW-15DAYS',
    end = 'NOW',
    limit = 1,
    min_ = 1,
    percentcloud = 1,
    read = False
    ):
    
    files = os.listdir()
    coords = get_coordinates(table, condition, N)
    if type(coords) == list:
        for coord in coords:
            coords_str = coord2wkt(coord, form, p)
            r = download_sent2(
                coords_str,
                user,
                passw,
                link,
                begin,
                end,
                limit,
                min_,
                percentcloud
            )

    elif type(coords) == dict:
        coords_str = coord2wkt(coords, form, p)
        r = download_sent2(
                coords_str,
                user,
                passw,
                link,
                begin,
                end,
                limit,
                min_,
                percentcloud
            )
    else:
        return print('Algo deu errado. (cloud2sat)')
    if r == True:
        newfiles = [file for file in os.listdir() if file not in files]
        return newfiles, coords
    else:
        return False

def extract(file):
    print('Extraindo arquivos.')
    #files = os.listdir()
    newfiles = list()
    if type(file) == list or type(file) == tuple:
        for filename in tqdm(file):
            if filename[-4:] != '.zip' and filename[-4:] != '.rar':
                filename += '.zip'

            Archive(filename).extractall('')
            #os.remove(filename)
            newfiles.append(filename[:-4] + '.SAFE')
    elif type(file) == str:
        if file[-4:] != '.zip' and filename[-4:] != '.rar':
            file += '.zip'

        Archive(file).extractall('')
        #os.remove(file)
        newfiles.append(file[:-4] + '.SAFE')
    else:
        return print('Arquivo inválido. (extract)')
    
    #newfiles = [file for file in os.listdir() if file not in files]
    return newfiles

def into_bandspath(filename):
    print('Acessando diretório.')
    bandpath = filename + '/GRANULE/'
    bandpath += os.listdir(bandpath)[0] + '/IMG_DATA/'
    return bandpath

def get_bandspath(filename, bands = ['B04', 'B08']):
    print('Encontrando imagens.')
    bandpath = into_bandspath(filename)
    paths = list()
    if type(bands) == str:
        bands = [bands]
    if type(bands) == list or type(bands) == set or type(bands) == tuple:
        for band in os.listdir(bandpath):
            if any(oneband in band for oneband in bands):
                print('Imagem {} selecionada.'.format(band))
                imgpath = bandpath + band
                paths.append(imgpath)
        return paths

def get_band(bandpath):
    imgs = list()
    if type(bandpath) == str:
        bandpath = [bandpath]
    if type(bandpath) == list or type(bandpath) == set or type(bandpath) == tuple:
        for band in bandpath:
            img = rasterio.open(band).read()
            imgs.append(img)
        return imgs
    else:
        return print('Diretório inválio. (get_band)')

def oneline_img(img):
    """
    input: x
    output: y
    """
    lineimg = list()
    if type(img) == list or type(img) == tuple or str(type(img)) == "<class 'numpy.ndarray'>":
        if type(img[0]) == list or str(type(img[0])) == "<class 'numpy.ndarray'>":
            for line in img:
                for pix in line:
                    lineimg.append(pix)
            return np.array(lineimg)
    else:
        return print('Imagem inválida. Verifique se o objeto é uma matriz. (oneline_img)')

def zip2vec(
    files,
    bands_ = ['B04', 'B05', 'B06']
    ):

    extract(files)
    allimgs = list()
    if type(files) != list:
        #listed = False
        files = [files]

    for filename in files:
        bandpath = into_bandspath(filename)
        bands = get_band(bandpath, bands_)
        imgs = list()
        for img in bands:
            lineimg = oneline_img(img)
            imgs.append(lineimg)
    allimgs.append(imgs)

    #if not listed:
    #    return all_imgs[0]

    return all_imgs

def cloud2vec(
    table,
    N = 0,
    condition = None,
    form = 'POLYGON',
    p = 100,
    user = 'thalesfreitaz',
    passw = 'sisustenido',
    link = 'https://scihub.copernicus.eu/dhus',
    begin = 'NOW-15DAYS',
    end = 'NOW',
    bands = ['B02', 'B03', 'B04']
    ):

    files = cloud2sat(table, N, condition, form, p, user, passw, link, begin, end)
    if len(files) != 0:
        imgs = zip2vec(files, bands)
        return imgs

    else:
        print('Nenhum arquivo foi baixado. (cloud2vec)')

def cloud2rgbvec(
    table,
    N = 0,
    condition = None,
    form = 'POLYGON',
    p = 100,
    user = 'thalesfreitaz',
    passw = 'sisustenido',
    link = 'https://scihub.copernicus.eu/dhus',
    begin = 'NOW-15DAYS',
    end = 'NOW',
    ):

    bands = ['B02', 'B03', 'B04']
    imgs = cloud2vec(table, N, condition, form, p, user, passw, link, begin, end, bands)
    
    data = list()
    for img in imgs:
        vecimg = nimg.rgb2vec(imgs)
        data.append(vecimg)

    return data

def get_zone(epgs):
    print('Requerindo sistema de referência de coordenadas...')
    if type(epgs) == int:
        epgs = str(epgs)
    if type(epgs) != str:
        return print('Entrada de epgs inválida. (get_zone)')
    url = 'http://spatialreference.org/ref/epsg/'
    browser = robobrowser.RoboBrowser(history=True, parser='html.parser')
    browser.open(url)
    form = browser.get_form()
    form['search'].value = epgs
    browser.submit_form(form, submit=form['search'])
    zone = str(browser.find_all('li')[0]).split('<')[3][-3:]
    z = int(zone[:2])
    n = zone[-1]
    return (z, n)

def clip_raster(filepath, coords, out = None, read = False):
    #print('Recortando imagem.')
    if out == None:
        out_tif = filepath.split('.')[0] + 'clipped' + '.' + filepath.split('.')[1]
    else:
        out_tif = out
    data = rasterio.open(filepath)
    epsg = data.read_crs().data['init'][5:]
    bounds = data.bounds
    zone = get_zone(epsg)
    #print('Coordinates:', coords)
    tcoords = np.array(coords['coordinates']).T
    #print('Transpose:', tcoords)
    bounds = data.bounds
    #print('Tamanho do array:', tcoords.shape[0])
    minx, miny, maxx, maxy = min(tcoords[0])[0][0], min(tcoords[1])[0][0], max(tcoords[0])[0][0], max(tcoords[1][0][0])
    #print(minx, miny, maxx, maxy)
    bbox = box(minx, miny, maxx, maxy)
    geo = gpd.GeoDataFrame({'geometry': bbox}, index=[0], crs=from_epsg(4326))  
    geo = geo.to_crs(crs = data.read_crs().data)

    coords_ = [json.loads(geo.to_json())['features'][0]['geometry']]

    out_img, out_transform = mask.mask(dataset=data, shapes=coords_, crop=True)
    out_meta = data.meta.copy()
    epsg_code = int(data.crs.data['init'][5:])
    out_meta.update({"driver": "GTiff",
                 "height": out_img.shape[1],
                 "width": out_img.shape[2],
                 "transform": out_transform,
                 "crs": pycrs.parser.from_epsg_code(epsg_code).to_proj4()}
               )
    #print('Criando imagem recortada.')
    with rasterio.open(out_tif, "w", **out_meta) as dest:
        dest.write(out_img)
    
    if read:
        clipped = rasterio.open(out_tif).read()
        return clipped

stop = [
    'nao',
    'não',
    'parar',
    'not',
    'n',
    'stop',
    'chega',
    'cancelar'
]

def join_bands(imgs):
    keep = True
    rgb = list()
    if type(imgs) == list:
        if any(len(imgs[i][0]) > 1000 for i in range(len(imgs))):
            
            ans = input('O tamanho das imagens é grande. Deseja continuar?').split()
            if any(word in stop for word in ans):
                keep = False
        if keep:
            for line in tqdm(range(len(imgs[0][0]))):
                rgb_line = list()
                for pix in range(len(imgs[0][0][0])):
                    rgb_pix = [imgs[i][0][line][pix] for i in range(len(imgs))]
                    rgb_line.append(rgb_pix)
                rgb.append(rgb_line)

    elif str(type(imgs)) == "<class 'numpy.ndarray'>":
    
        for line in tqdm(range(imgs.shape[1])):
            rgb_line = list()
            for pix in range(imgs.shape[2]):
                rgb_pix = [imgs[i][0][line][pix] for i in range(imgs.shape[0])]
                rgb_line.append(rgb_pix)
            rgb.append(rgb_line)
            
    return rgb

def rgb_clipped(pth, coords):
    filepaths = get_bandspath(path, bands = ['B04', 'B03', 'B02'])[::-1]
    #clips = list()
    for fp in filepaths:
        clipped = clip_raster(fp, coords)
        #clips.append(clipped)
    #rgb = clips
    #rgb = join_bands(clips)
    return True

def cloud2clip(
    table,
    N = 0,
    condition = None,
    form = 'POLYGON',
    p = 100,
    user = 'thalesfreitaz',
    passw = 'sisustenido',
    link = 'https://scihub.copernicus.eu/dhus',
    begin = 'NOW-15DAYS',
    end = 'NOW',
    limit = 1,
    min_ = 1,
    percentcloud = (0, 5),
    out = None,
    read = False,
    ):
    files, coords = cloud2sat(
        table,
        N,
        condition,
        form,
        p,
        user,
        passw,
        link,
        begin,
        end,
        limit,
        min_,
        percentcloud,
        read,
    )
    if files != False:
        files = extract(files)
        if type(files) != list:
            #listed = False
            files = [files]

        if read:
            all_clips = list()

        for filename in files:
            if read:
                clips = list()
            bandpath = get_bandspath(filename, bands = ['B02', 'B03', 'B04'])

            for band in bandpath:
                clip = clip_raster(band, coords, out,  read)
                if read:
                    clips.append(clip)
            if read:
                all_clips.append(clips)
        
        if read:
            return all_clips
    else:
        return False

def query_sentinel(
    key_json = 'Nagro-Satellite-74a8a35144bd.json',
    project_id = 'nagro-satellite',
    start = None,
    end = None,
    tile = None,
    cloud = None,
    bands = None
    ):
    BASE_URL = 'http://storage.googleapis.com/'
    if type(bands) != list:
        bands = [bands]
    for band in tqdm(bands):
        credentials = service_account.Credentials.from_service_account_file(key_json)
        client = bigquery.Client(credentials=credentials, project=project_id)
        query = client.query("""
                    SELECT * FROM `bigquery-public-data.cloud_storage_geo_index.sentinel_2_index` 
                        WHERE (mgrs_tile = '{t}' AND 
                        CAST(SUBSTR(sensing_time, 1, 10) AS DATE) >= CAST('{s}' AS DATE) AND 
                        CAST(SUBSTR(sensing_time, 1, 10) AS DATE) < CAST('{e}' AS DATE))
                    """.format(t=tile, s=start, e=end))
        results = query.result()
        df = results.to_dataframe()
        #print(df)
        good_scenes = []
        for i, row in df.iterrows():
            #print(row['product_id'], '; cloud cover:', row['cloud_cover'])
            if float(row['cloud_cover']) <= cloud:
                good_scenes.append(row['base_url'].replace('gs://', BASE_URL)+'/GRANULE/'+row['granule_id']+'/IMG_DATA/'+row['granule_id'][4:10]+'_'+row['datatake_identifier'][5:20]+'_'+band+'.jp2')
                url = row['base_url'].replace('gs://', BASE_URL)+'/GRANULE/'+row['granule_id']+'/IMG_DATA/'+row['granule_id'][4:10]+'_'+row['datatake_identifier'][5:20]+'_'+band+'.jp2'
                path = row['sensing_time'][:10]
                if not os.path.exists('./imagens-de-satelite/' + tile + '/' + path):
                    os.makedirs('./imagens-de-satelite/' + tile + '/' + path)
                #print(url)
                r = requests.get(url, stream=True)
                filename = url.split("/")[-1]
                with open('./imagens-de-satelite/' + tile + '/' + path + '/' + filename, 'wb') as f:
                    f.write(r.content)
                with open('./imagens-de-satelite/' + tile + '/' + path + '/metadata.py', 'w') as f:
                    f.write('data: {\n')
                    for col in df.columns:
                        f.write('{}: {},\n'.format(col, row[col]))
                    f.write('}')

# This is a mistake. Don't use.
def cloud2bigquery(
    table,
    N = 0,
    condition = None,
    form = 'POLYGON',
    p = 100,
    user = 'thalesfreitaz',
    passw = 'sisustenido',
    link = 'https://scihub.copernicus.eu/dhus',
    begin = 'NOW-15DAYS',
    end = 'NOW',
    limit = 1,
    min_ = 1,
    percentcloud = 1,
    read = False,
    key_json = 'Nagro-Satellite-74a8a35144bd.json',
    project_id = 'nagro-satellite',
    bands = None
    ):
    
    result = dict()

    files = os.listdir()
    coords = get_coordinates(table, condition, N)

    if type(coords) == dict:
        coords_str = coord2wkt(coords, form, p)
        products = get_sent2_products(
                coords_str,
                user,
                passw,
                link,
                begin,
                end,
                limit,
                min_,
                percentcloud
            )
        result 


    else:
        return print('Algo deu errado. (cloud2sat)')

    if type(products) == list:
        tiles = {product[list(product)[0]]['tileid'] for product in products}
        for tile in tiles:
            query_sentinel(key_json,
            project_id = project_id,
            start = begin,
            end = end,
            tile = tile,
            cloud = percentcloud,
            bands = bands)
        return tiles
    else:
        tile = products[list(products)[0]]['tileid']
        query_sentinel(key_json,
        project_id = project_id,
        start = begin,
        end = end,
        tile = tile,
        cloud = percentcloud,
        bands = bands)
        return tile