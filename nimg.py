from sklearn.decomposition import PCA
import sklearn.cluster as cl
import numpy as np

def rgb2vec(imgarray):
    pca = PCA(1)
    if str(type(imgarray)) == "<class 'numpy.ndarray'>":
        return pca.fit_transform(imgarray)
    elif type(imgarray) == list or type(imgarray) == tuple:
        return pca.fit_transform(np.array(imgarray).T)
    else:
        return print('Formato inserido inválido. (rgb2vec)')

def print_classes(X, output):
    for x, c in zip(X, output):
        print(x,'foi classificado como:', c)